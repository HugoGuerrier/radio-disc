
// ========== PROPOSITION POOL ==========

/**
 * Propose a music : add the music to the music pool.
 */
function makeProposition() {
	let videoId = document.getElementById("video-id").value;
	let proposition = { method: "POST" };
	
	fetch(window.location.href + "pool?video_id=" + videoId, proposition)
	.then(function(response) {
	  	return response.json();
	})
	.then(function(json) {
		if(json.success) {
			// Display success
			alert("Musique ajoutée !");
		} else {
			// Display failure
			alert(json.errorMessage);
		}
	});
}

/**
 * Vote for a music proposition
 */
function voteProposition(propVideoId) {
	let vote = { method: "PUT" };

	fetch(window.location.href + "pool?video_id=" + propVideoId, vote)
	.then(function(response) {
		return response.json();
	})
	.then(function(json) {
		if(json.success) {
			// Display success
			alert("A voté !");
		} else {
			// Display failure
			alert(json.errorMessage);
		}
	});
}

/**
 * Delete a music proposition
 * @param {*} propVideoId The videoId of the proposition 
 */
function deleteProposition(propVideoId) {
	let del = { method: "DELETE" };

	fetch(window.location.href + "pool?video_id=" + propVideoId, del)
	.then(function(response) {
		return response.json();
	})
	.then(function(json) {
		if(json.success) {
			// Display success
			alert("La proposition est bien supprimée.");
		} else {
			// Display failure
			alert(json.errorMessage);
		}
	});
}

/**
 * Display the proposition pool and allows the users to vote for propositions and admins to delete propositions
 */
function displayPropPool() {
	let proposition = { method: "GET" };
	
	fetch(window.location.href + "pool", proposition)
	.then(function(response) {
	  	return response.json();
	})
	.then(function(json) {
		// Box of the pool
		let propListDiv = document.getElementById("proposition-list");
		propListDiv.className = "d-flex flex-column";
		propListDiv.innerHTML = "";

		for(let prop of json) {
			// Proposition description
			let propDesc = document.createElement("p");
			propDesc.className = "align-middle"
			propDesc.innerHTML = prop.title + " - " + prop.channel;

			// Vote button
			let propVoteBtn = document.createElement("input");
			propVoteBtn.type = "button";
			propVoteBtn.value = "Voter";
			propVoteBtn.className = "ms-2";
			propVoteBtn.onclick = function(){ voteProposition(prop.videoId); };

			// Delete button
			let propDeleteBtn = document.createElement("input");
			propDeleteBtn.type = "button";
			propDeleteBtn.value = "Supprimer";
			propDeleteBtn.style.visibility = "hidden";
			propDeleteBtn.className = "delete-button btn btn-danger ms-2";
			propDeleteBtn.onclick = function(){ deleteProposition(prop.videoId); };

			// Proposition box
			let propDiv = document.createElement("div"); 
			propDiv.style = "display:inline-flex"
			propDiv.className = "mt-2"
			propDiv.appendChild(propDesc);
			propDiv.appendChild(propVoteBtn);
			propDiv.appendChild(propDeleteBtn);

			// Add the proposition box to the list 
			propListDiv.appendChild(propDiv);
		}
		adminView();
	});
}


// ========== MUSIC QUEUE ==========


/**
 * Delete a music from the queue
 * @param musicVideoId The videoId of the music
 */
function deleteMusicFromQueue(musicVideoId) {
	let del = { method: "DELETE" };

	fetch(window.location.href + "queue?video_id=" + musicVideoId, del)
	.then(function(response) {
		return response.json();
	})
	.then(function(json) {
		if(json.success) {
			// Display success
			alert("La musique est bien supprimée de la file d'attente.");
		} else {
			// Display failure
			alert(json.errorMessage);
		}
	});
}


/**
 * Display the music queue and allows admins to delete musics
 */
 function displayMusicQueue() {
	let queue = { method: "GET" };
	
	fetch(window.location.href + "queue", queue)
	.then(function(response) {
	  	return response.json();
	})
	.then(function(json) {
		// Box of the queue
		let queueListDiv = document.getElementById("queue-list");
		queueListDiv.innerHTML = "";

		for(let music of json) {
			// Music description
			let musicDesc = document.createElement("p");
			musicDesc.innerHTML = music.title + " - " + music.channel;

			// Delete button
			let musicDeleteBtn = document.createElement("input");
			musicDeleteBtn.type = "button";
			musicDeleteBtn.value = "Supprimer";
			musicDeleteBtn.style.visibility = "hidden";
			musicDeleteBtn.className = "delete-button btn btn-danger ms-2";
			musicDeleteBtn.onclick = function(){ deleteMusicFromQueue(music.videoId); };

			// Music box
			let musicDiv = document.createElement("div"); 
			musicDiv.style = "display:inline-flex"
			musicDiv.appendChild(musicDesc);
			musicDiv.appendChild(musicDeleteBtn);

			// Add the music box to the list
			queueListDiv.appendChild(musicDiv);
		}
	});
	adminView();
}

// ========== ADMIN ==========

/**
 * display admin interface
 */
function adminView(){
	let display = { method: "GET" };
	
	fetch(window.location.href + "dog?display=ask")
	.then(function(response) {
	  	return response.json();
	})
	.then(function(response){
		if(!response["success"]){
			document.getElementById("admin_login").style.display = "none";
			document.getElementById("admin_logged").style.display = "block";
			for (deleteBtn of document.getElementsByClassName("delete-button")){
				deleteBtn.style.visibility = "visible";
			}
			
		} else {
			document.getElementById("admin_login").style.display = "block";
			document.getElementById("admin_logged").style.display = "none";
			for (deleteBtn of document.getElementsByClassName("delete-button")){
				deleteBtn.style.visibility = "hidden";
			}
		}
	});
	
}


/**
 * Log an admin
 */

 function logAdmin() {
	let login = { method: "GET" };
	let username = document.getElementById("admin_username").value;
	document.getElementById("admin_username").value = "";

	let pwd = document.getElementById("admin_password").value;
	document.getElementById("admin_password").value = "";
	
	fetch(window.location.href + "dog?username=" + username + "&password=" + sha512(pwd))
	.then(function(response) {
	  	return response.json();
	})
	.then(function(response){
		adminView();
	});
}

/**
 * Log out an admin
 */
function logOutAdmin(){
	let login = { method: "GET" };
	
	fetch(window.location.href + "dog?logout=true")
	.then(function(response) {
	  	return response.json();
	})
	.then(function(response){
		adminView();
	});

}

/**
 * Sign in an admin
 */
function signInAdmin() {
	let username = document.getElementById("new_admin_username").value;
	let mail = document.getElementById("new_admin_mail").value;
	let pwd = document.getElementById("new_admin_password").value;
	let confirm_pwd = document.getElementById("new_admin_confirm_password").value;
	let level = document.getElementById("new_admin_level").value;

	let proposition = { method: "POST" };
	
	fetch(window.location.href + "dog?username=" + username + "&mail=" + mail + "&password=" + sha512(pwd) + "&confirm_password=" + sha512(confirm_pwd) + "&level=" + level, proposition)
	.then(function(response) {
	  	return response.json();
	})
	.then(function(json) {
		if(json.success) {
			// Display success
			alert("Admin ajouté !");
		} else {
			// Display failure
			alert(json.errorMessage);
		}
		adminView();
	});
}

/**
 * Delete an admin
 */
function removeAdmin(){

	let proposition = { method: "DELETE" };
	
	fetch(window.location.href + "dog?delete=true", proposition)
	.then(function(response) {
	  	return response.json();
	})
	.then(function(json) {
		if(json.success) {
			// Display success
			alert("Admin supprimé !");
		} else {
			// Display failure
			alert(json.errorMessage);
		}
		adminView();
	});
}

/**
 * Update an admin
 */
function updateAdmin(){
	let res = "dog?"
	let new_mail = document.getElementById("update_admin_mail").value;
	if(new_mail.length > 0){
		res = res + "new_mail=" + new_mail + "&";
	}
	let old_pwd = document.getElementById("old_admin_password").value;
	if(old_pwd.length > 0){
		res = res + "old_pwd=" + sha512(old_pwd) + "&";
	}
	let pwd = document.getElementById("update_admin_password").value;
	if(pwd.length > 0){
		res = res + "password=" + sha512(pwd) + "&";
	}
	let confirm_pwd = document.getElementById("update_admin_confirm_password").value;
	if(confirm_pwd.length > 0){
		res = res + "confirm_password=" + sha512(confirm_pwd) + "&";
	}

	let proposition = { method: "PUT" };

	fetch(window.location.href + res, proposition)
	.then(function(response) {
	  	return response.json();
	})
	.then(function(json) {
		if(json.success) {
			// Display success
			alert("Informations mises à jour !");
		} else {
			// Display failure
			alert(json.errorMessage);
		}
		adminView();
	});
}