package com.radio_disc.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * This servlet help to constrol remotely the discord bot
 */
@WebServlet("/bot")
public class BotServlet extends HttpServlet {
	
	// ===== Attributes =====
	
	
	private static final long serialVersionUID = 767815281752758960L;

	
	// ===== Constructors =====
	
	
	/**
     * Create a new bot servlet to control the bor
     */
    public BotServlet() {
        super();
    }

    
    // ===== HTTP methods =====
    
    
	/**
	 * Get the current bot state
	 */
    @Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO : Get the current bot state
	}

    
	/**
	 * Start the discord bot
	 */
    @Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO : Start the dicord bot !!! ADMIN !!!
	}
    

    /**
     * Update the bot state or behavior
     */
	@Override
	protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO : Parse the command and do it on the bot !!! ADMIN !!!
	}

	
	/**
	 * Stop the bot from running
	 */
	@Override
	protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO : Stop the bot !!! ADMIN !!!
	}

}
