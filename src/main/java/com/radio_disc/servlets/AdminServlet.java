package com.radio_disc.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONObject;

import com.radio_disc.database.AdminDB;
import com.radio_disc.exceptions.DatabaseException;
import com.radio_disc.models.Admin;
import com.radio_disc.utils.WebUtils;

/**
 * This servlet handle all operation to manage admin
 * 
 * @author Hugo Guerrier
 * @author Erik Vienne
 */
@WebServlet("/dog")
public class AdminServlet extends HttpServlet {

	// ===== Attributes =====
	
	
	private static final long serialVersionUID = 6350368797678203168L;
	
	
	// ===== Constructors =====
	
	
    /**
     * Create a new admin management servlet
     */
    public AdminServlet() {
        super();
    }
    
    
    // ===== HTTP methods =====

    
    /**
     * Log an administrator in
     */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO : Verify the login and create the admin session
		HttpSession session = request.getSession();
		
		// Initialize the JSON with a success
		JSONObject jsonResp = WebUtils.getDefaultSuccessResponse();
		
		if(request.getParameter("display") != null) {
			if(session.getAttribute("admin") != null) {
				// Can't display login
				jsonResp = WebUtils.getDefaultFailResponse("Can't display login : already logged");
			}
		} else if (request.getParameter("logout") != null) {
			session.invalidate();
		} else {
			if(session.getAttribute("admin") == null) {
				// TODO update + delete page
				String username = request.getParameter("username");
				String pwd = request.getParameter("password");
				Admin current = AdminDB.getAdminByPseudo(username);
				if(current != null) {
					if(current.getPassword().equals(pwd)) {
						session.setAttribute("admin", current);
						//response to json
					} else {
						// Can't find the admin
						jsonResp = WebUtils.getDefaultFailResponse("Can't log : Invalid password");
					}				
				} else {
					// Can't find the admin
					jsonResp = WebUtils.getDefaultFailResponse("Can't log : Invalid userame");
				}
				
			} else {
				// Already logged in
				jsonResp = WebUtils.getDefaultFailResponse("Can't log : Already logged in");
			}
		}
				
		// Append the JSON result to the response
		response.getWriter().append(jsonResp.toJSONString());
	}
	
	/**
	 * Create a new administrator
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO : Verify the creation fields and create the admin !!! ADMIN !!!
		
		// Initialize the JSON with a success
		JSONObject jsonResp = WebUtils.getDefaultSuccessResponse();
			
		HttpSession session = request.getSession();

			PrintWriter out = response.getWriter();
			String username = request.getParameter("username");
			String mail = request.getParameter("mail");
			String pwd = request.getParameter("password");
			String confirm_pwd = request.getParameter("confirm_password");
			int level = Integer.parseInt(request.getParameter("level"));
			System.out.println("" + username + ", " + mail + "," + pwd + ", " + confirm_pwd + ", " + level);
			
			if(AdminDB.getAdminByPseudo(username) == null) { // admin doesn't exist in DB
				if(isValidEmailAddress(mail)) { //mail format is right
					if(pwd.equals(confirm_pwd) && !pwd.equals("")) { // passwords are the same and not null
						try {
							String hash_pwd = pwd;
							AdminDB.addAdmin(new Admin(username, mail, hash_pwd, level));
							Admin current = AdminDB.getAdminByPseudo(username);
							
							session.setAttribute("admin", current);
							
						} catch (DatabaseException e) {
							// TODO Auto-generated catch block
							jsonResp = WebUtils.getDefaultFailResponse("Can't add admin : Admin already present in the database");
						}
					}
					else {
						// "Password not matching"
						jsonResp = WebUtils.getDefaultFailResponse("Can't create admin: password and confirm password are different");
					}
				} else {
					//invalid mail address
					jsonResp = WebUtils.getDefaultFailResponse("Can't create admin: mail is invalid");
				}
			}
			else {
				//username already used
				jsonResp = WebUtils.getDefaultFailResponse("Can't create admin: user already used");

						
		}
		// Append the JSON result to the response
		response.getWriter().append(jsonResp.toJSONString());
	}

	
	/**
	 * Update an administrator
	 */
	@Override
	protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO : Update the current admin !!! ADMIN !!!
		// Initialize the JSON with a success
		JSONObject jsonResp = WebUtils.getDefaultSuccessResponse();
		HttpSession session = request.getSession();
		Admin current = (Admin) session.getAttribute("admin");
		String username = current.getPseudo();
		
		String mail = request.getParameter("new_mail");
		String old_pwd = request.getParameter("old_pwd");
		String pwd = request.getParameter("password");
		String confirm_pwd = request.getParameter("confirm_password");
		
		
		if(current != null) {
			if(mail != null) {
				if(isValidEmailAddress(mail)) {
					try {
						Admin newAdmin = current;
						newAdmin.setMail(mail);
						AdminDB.updateAdmin(current, newAdmin);
						current = AdminDB.getAdminByPseudo(username);
						session.setAttribute("admin", current);
					} catch (DatabaseException e) {
						// TODO Auto-generated catch block
						jsonResp = WebUtils.getDefaultFailResponse("Can't update admin : database error");
					}
				} else {
					//mail not valid
					jsonResp = WebUtils.getDefaultFailResponse("Can't update : email address is not valid");
				}
			}
			//----------------
			if(old_pwd != null && current.getPassword().equals(old_pwd)) {
				if(pwd != null && confirm_pwd != null && pwd.equals(confirm_pwd)) {
					try {
						Admin newAdmin = current;
						newAdmin.setPassword(pwd);
						AdminDB.updateAdmin(current, newAdmin);
						current = AdminDB.getAdminByPseudo(username);
						session.setAttribute("admin", current);
					} catch (DatabaseException e) {
						// TODO Auto-generated catch block
						jsonResp = WebUtils.getDefaultFailResponse("Can't update admin : database error");
					}
				} else {
					//new passwords are not matching
					jsonResp = WebUtils.getDefaultFailResponse("Can't update : new passwords are not matching");
				}
			} else {
				//old password not valid
				jsonResp = WebUtils.getDefaultFailResponse("Can't update : old password is wrong");
			}
			
		} else {
			//admin not logged
			jsonResp = WebUtils.getDefaultFailResponse("Can't update : admin not logged");
		}
		
		// Append the JSON result to the response
		response.getWriter().append(jsonResp.toJSONString());
		
	}
	
	
	/**
     * Delete an administrator
     */
    @Override
	protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO : Delete an admin from the database, the deleter must be more poweful !!! ADMIN !!!
    	// Initialize the JSON with a success
		JSONObject jsonResp = WebUtils.getDefaultSuccessResponse();
			
		HttpSession session = request.getSession();
		
		if(session.getAttribute("admin") != null && request.getParameter("delete") != null) {
			Admin current = (Admin) session.getAttribute("admin");
			AdminDB.deleteAdmin(current);
			session.invalidate();
		} else {
			//can't remove
			jsonResp = WebUtils.getDefaultFailResponse("Can't remove");
		}
		
		// Append the JSON result to the response
		response.getWriter().append(jsonResp.toJSONString());
    			
    	
	}
    
    public boolean isValidEmailAddress(String email) {
        String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(email);
        return m.matches();
 }
    


}
