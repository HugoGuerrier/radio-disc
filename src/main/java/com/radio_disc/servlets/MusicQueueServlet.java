package com.radio_disc.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.radio_disc.database.MusicQueue;
import com.radio_disc.models.Music;
import com.radio_disc.utils.WebUtils;

/**
 * This servlet helps to manage the music queue
 * 
 * @author Emilie Siau
 */
@WebServlet("/queue")
public class MusicQueueServlet extends HttpServlet {
	
	// ===== Attributes =====
	
	
	private static final long serialVersionUID = -6144662342102008603L;

	
	// ===== Constructors =====
	
	
	/**
     * Create a new music queue servlet
     */
    public MusicQueueServlet() {
        super();
    }
    
    
    // ===== HTTP methods =====

    
    /**
     * Get the music queue state
     */
	@SuppressWarnings("unchecked")
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// Get the music queue
		List<Music> queue = MusicQueue.getQueue();
		// Prepare the result array
		JSONArray res = new JSONArray();
		
		if(queue != null) {
			// Add each music from the queue to the result
			for(Music music : queue) {
				res.add(music.toJson());
			}
		}
		
		// Append the result array to the response
		resp.getWriter().append(res.toJSONString());
	}
	
	
	/**
     * Delete a music from the playing queue (authorized for administrators only)
     */
    @Override
	protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		// Initialize the JSON with a success
		JSONObject jsonResp = WebUtils.getDefaultSuccessResponse();

		String videoId = req.getParameter("video_id");
		HttpSession session = req.getSession(); // Only administrators get sessions
		
		if (session.getAttribute("admin") != null) {
			if (videoId != null) {
				
				// Find the corresponding music
				Music music= MusicQueue.getMusicByVideoId(videoId);
				
				if (music != null) {
					// Delete the proposition
					if (!MusicQueue.deleteMusic(music)) {
						// Shouldn't occur : MongoDB error
						jsonResp = WebUtils.getDefaultFailResponse("Can't delete : MongoDB database error");
					}				
				} else {
					// Can't find the music
					jsonResp = WebUtils.getDefaultFailResponse("Can't delete : the music isn't in the music queue");				
				}
				
			} else {
				// The request is malformed
				jsonResp = WebUtils.getDefaultFailResponse("Can't delete : malformed request on video_id");
			}
		} else {
			// The user isn't an administrator
			jsonResp = WebUtils.getDefaultFailResponse("Can't delete : not an administrator");
		}
		
		// Append the JSON result to the response
		resp.getWriter().append(jsonResp.toJSONString());
	
	}

}
