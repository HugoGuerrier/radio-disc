package com.radio_disc.servlets;

import java.io.File;
import java.io.IOException;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;

import com.radio_disc.database.AdminDB;
import com.radio_disc.exceptions.DatabaseException;
import com.radio_disc.models.Admin;
import com.radio_disc.utils.Config;
import com.radio_disc.utils.Logger;

/**
 * This servlet is only used by the http server when it starts
 */
@WebServlet("/configurator")
public class ConfiguratorServlet extends HttpServlet {

	// ===== Attributes =====
	
	
	private static final long serialVersionUID = -260928794743450329L;
	
	
	// ===== Constructors =====
       
	
    /**
     * Create a new configurator
     */
    public ConfiguratorServlet() {
        super();
    }
    
    
    // ===== Servlet methods =====
    
    
    @Override
	public void init() throws ServletException {
    	// Call the super method
		super.init();
		
		// Get the application base path
		ServletContext context = this.getServletContext();
		Config.basePath = context.getRealPath("./");
		
		// Configure the server here
		Config.videoDir = new File(Config.basePath + "videos");
		Config.verbose = true;
		Config.env = Config.DEV_ENV;
		
		// Init the application logger
		try {
			Logger.init();
		} catch (IOException e) {
			System.err.println("[ERROR] = Cannot initialize the logger");
		}
		
		// Display the configuration
		if(Config.verbose) {
			Config.display();
		}
		
		Admin original = AdminDB.getAdminByPseudo("admin");
		if(original == null) {
			Admin admin = new Admin("admin", "admin@admin.admin", "c7ad44cbad762a5da0a452f9e854fdc1e0e7a52a38015f23f3eab1d80b931dd472634dfac71cd34ebc35d16ab7fb8a90c81f975113d6c7538dc69dd8de9077ec", 0);
			try {
				AdminDB.addAdmin(admin);
			} catch (DatabaseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}
