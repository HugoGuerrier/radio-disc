package com.radio_disc.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.radio_disc.database.MusicPool;
import com.radio_disc.exceptions.DatabaseException;
import com.radio_disc.exceptions.YoutubeException;
import com.radio_disc.models.Proposition;
import com.radio_disc.utils.Config;
import com.radio_disc.utils.WebUtils;
import com.radio_disc.utils.YoutubeAPI;

/**
 * Servlet to handle the music proposition pool
 * 
 * @author Hugo Guerrier
 * @author Emilie Siau
 */
@WebServlet("/pool")
public class MusicPoolServlet extends HttpServlet {

	// ===== Attributes =====
	
	
	private static final long serialVersionUID = -2472288924355405397L;
	
	
	// ===== Constructors =====
	
	
    /**
     * Create a new music pool servlet
     */
    public MusicPoolServlet() {
        super();
    }
    
    
    // ===== HTTP methods =====

    
    /**
	 * Get the music list with the number of votes
	 */
    @Override
    @SuppressWarnings("unchecked")
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    	// Get the proposition list
		List<Proposition> props = MusicPool.getPool();
		// Prepare the result array
		JSONArray res = new JSONArray();
		
		if(props != null) {
			// Add each proposition from the pool to the result
			for(Proposition prop : props) {
				res.add(prop.toJson());
			}
		}
		
		// Append the result array to the response
		resp.getWriter().append(res.toJSONString());
	}

    
    /**
	 * Add a music to the proposition pool
	 */
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		// Initialize the JSON with a success
		JSONObject jsonResp = WebUtils.getDefaultSuccessResponse();
		
		try {

			// Create a proposition from the video id
			String videoId = req.getParameter("video_id");
			YoutubeAPI youtubeAPI = new YoutubeAPI(Config.youtubeAPIKey);
			Proposition prop = youtubeAPI.getVideoInfo(videoId);
			
			if (videoId != null) {
				
				if (prop == null) {
					// Can't find the proposition
					jsonResp = WebUtils.getDefaultFailResponse("Can't vote : can't find corresponding video");
					
				} else {
					// Add the proposition to the proposition pool
					MusicPool.addProposition(prop);
				}
				
			} else {
				// The request is malformed
				jsonResp = WebUtils.getDefaultFailResponse("Can't add music : malformed request on video_id");
			}
						
		} catch (YoutubeException e) {
			// The new YoutubeAPI failed
			jsonResp = WebUtils.getDefaultFailResponse("Can't add music : Youtube API is down");
					
		} catch (DatabaseException e) {
			// Database failure
			jsonResp = WebUtils.getDefaultFailResponse("Can't add music : the video already exists in the music pool");
			
		} finally {
			// Append the JSON result to the response
			resp.getWriter().append(jsonResp.toJSONString());
		}
		
	}
	
	
	/**
	 * Vote for a music in the proposition pool
	 */
	@Override
	protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		// Initialize the JSON with a success
		JSONObject jsonResp = WebUtils.getDefaultSuccessResponse();
		
		try {
			
			String videoId = req.getParameter("video_id");
			
			if (videoId != null) {
				
				// Find the corresponding proposition
				Proposition prop = MusicPool.getPropositionByVideoId(videoId);
				
				if (prop != null) {
					// Vote for the proposition			
					if (!MusicPool.vote(prop)) {
						// Shouldn't occur : MongoDB error
						jsonResp = WebUtils.getDefaultFailResponse("Can't vote : MongoDB database error");
					}				
				} else {
					// Can't find the proposition
					jsonResp = WebUtils.getDefaultFailResponse("Can't vote : the video isn't in the music pool");				
				}
				
			} else {
				// The request is malformed
				jsonResp = WebUtils.getDefaultFailResponse("Can't vote : malformed request on video_id");
			}
			
		} catch (DatabaseException e) {
			// Database failure
			jsonResp = WebUtils.getDefaultFailResponse("Can't vote : the video isn't in the database");
			
		} finally {
			// Append the JSON result to the response
			resp.getWriter().append(jsonResp.toJSONString());
		}
		
	}

	
	/**
	 * Delete a music from the proposition pool (authorized for administrators only)
	 */
	@Override
	protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		// Initialize the JSON with a success
		JSONObject jsonResp = WebUtils.getDefaultSuccessResponse();

		String videoId = req.getParameter("video_id");
		HttpSession session = req.getSession();

		if (session.getAttribute("admin") != null) {
			if (videoId != null) {
				
				// Find the corresponding proposition
				Proposition prop = MusicPool.getPropositionByVideoId(videoId);
				
				if (prop != null) {
					// Delete the proposition
					if (!MusicPool.deleteProposition(prop)) {
						// Shouldn't occur : MongoDB error
						jsonResp = WebUtils.getDefaultFailResponse("Can't delete : MongoDB database error");
					}				
				} else {
					// Can't find the proposition
					jsonResp = WebUtils.getDefaultFailResponse("Can't delete : the video isn't in the music pool");				
				}
				
			} else {
				// The request is malformed
				jsonResp = WebUtils.getDefaultFailResponse("Can't delete : malformed request on video_id");
			}
		} else {
			// The user isn't an administrator
			jsonResp = WebUtils.getDefaultFailResponse("Can't delete : not an administrator");
		}
		// Append the JSON result to the response
		resp.getWriter().append(jsonResp.toJSONString());
	
	}	

}
