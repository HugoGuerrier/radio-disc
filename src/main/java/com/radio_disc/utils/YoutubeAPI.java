package com.radio_disc.utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.radio_disc.exceptions.YoutubeException;
import com.radio_disc.models.Proposition;

/**
 * Contains all needed methods for the youtube API
 * 
 * @author Hugo Guerrier
 */
public class YoutubeAPI {
	
	// ===== Attributes =====
	
	
	private String key;
	private final String testUrl = "https://youtube.googleapis.com/youtube/v3/videos?"
			+ "key=API_KEY"
			+ "&id=42";
	private final String videoUrl = "https://youtube.googleapis.com/youtube/v3/videos?"
			+ "key=API_KEY"
			+ "&id=VID_ID"
			+ "&part=snippet&part=contentDetails"
			+ "&maxResults=1";
	
	
	// ===== Constructor =====
	
	
	public YoutubeAPI(String key) throws YoutubeException {
		this.key = key;
		
		// Verify the connection
		if(!this.checkConnection()) {
			throw new YoutubeException("Cannot connect to the youtube API, check log for more details");
		}
	}
	
	
	// ===== Class methods =====
	
	
	/**
	 * Verify the connection to the youtube API
	 * 
	 * @return True if the connection is possible, false else
	 */
	public boolean checkConnection() {
		try {
			
			// Create the test URL and open the connection
			URL url = new URL(this.testUrl.replace("API_KEY", this.key));
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			
			// Set the request parameters
			connection.setRequestProperty("accept", "application/json");
			
			// Verify the result code
			if(connection.getResponseCode() == 200) {
				return true;
			} else {
				Logger.log("The youtube response code is : " + connection.getResponseCode(), Logger.ERROR);
			}
			
		} catch (MalformedURLException e) {
			
			Logger.log("Test URL is malformed", Logger.ERROR);
			Logger.log(e, Logger.ERROR);
			
		} catch (IOException e) {
			
			Logger.log("Http connection input stream cannot be opened", Logger.ERROR);
			Logger.log(e, Logger.ERROR);
			
		}
		
		// If the program reach this line, the connection failed
		return false;
	}
	
	
	/**
	 * Get the video information stored in a Proposition object
	 * 
	 * @param videoId The video id
	 * @return The proposition filled with the informations or null if there is an error
	 */
	public Proposition getVideoInfo(String videoId) {
		try {
			
			// Create the test URL and open the connection
			URL url = new URL(this.videoUrl.replace("API_KEY", this.key).replace("VID_ID", videoId));
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			
			// Set the request parameters
			connection.setRequestProperty("accept", "application/json");
			
			// Verify the result code
			if(connection.getResponseCode() == 200) {
				
				// Get the response reader
				InputStreamReader reader = new InputStreamReader(connection.getInputStream());
				
				// Parse the response
				JSONParser parser = new JSONParser();
				JSONObject jsonResponse = (JSONObject) parser.parse(reader);
				
				// Verify that there is a result
				JSONObject pageResult = (JSONObject) jsonResponse.get("pageInfo");
				Long totalResult = (Long) pageResult.get("totalResults");
				
				if(totalResult == 1) {
					
					// Prepare the result
					Proposition res = new Proposition();
					
					// Get the video item
					JSONObject item = (JSONObject) ((JSONArray) jsonResponse.get("items")).get(0);
					
					// Get the video details object
					JSONObject snippet = (JSONObject) item.get("snippet");
					JSONObject contentDetails = (JSONObject) item.get("contentDetails");
					
					// Fill the proposition information
					res.setVideoId(videoId);
					res.setScore(0);
					res.setTitle((String) snippet.get("title"));
					res.setChannel((String) snippet.get("channelTitle"));
					res.setDurationYoutubeFormat((String) contentDetails.get("duration"));
					
					if(((String) snippet.get("categoryId")).equals("10")) {
						res.setMusic(true);
					} else {
						res.setMusic(false);
					}
					
					// Return the result
					return res;
					
				}
				
			} else {
				Logger.log(this.videoUrl.replace("API_KEY", this.key).replace("VID_ID", videoId) + " : the youtube response code is : " + connection.getResponseCode(), Logger.ERROR);
			}
			
		} catch (MalformedURLException e) {
			
			Logger.log("Test URL is malformed", Logger.ERROR);
			Logger.log(e, Logger.ERROR);
			
		} catch (IOException e) {
			
			Logger.log("Http connection input stream cannot be opened", Logger.ERROR);
			Logger.log(e, Logger.ERROR);
			
		} catch (ParseException e) {
			
			Logger.log("The Youtube API response cannot be parsed in JSON", Logger.ERROR);
			Logger.log(e, Logger.ERROR);
			
		}
		
		// Return the default value
		return null;
	}
	
}
