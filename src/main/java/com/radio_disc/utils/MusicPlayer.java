package com.radio_disc.utils;

import java.util.Calendar;

import com.radio_disc.models.Music;

/**
 * This class contains all information about the currently played music
 * This class also handle the music pooling
 * 
 * @author Hugo Guerrier
 */
public class MusicPlayer {
	
	// ===== Attributes =====
	
	
	private Music currentMusic;
	private long startTime;
	
	private static MusicPlayer instance = null;
	
	
	// ===== Constructors =====
	
	
	private MusicPlayer() {
		this.currentMusic = null;
		this.startTime = 0;
	}
	

	public static MusicPlayer getInstance() {
		if(MusicPlayer.instance == null) {
			MusicPlayer.instance = new MusicPlayer();
		}
		
		return MusicPlayer.instance;
	}
	
	
	// ===== Getters =====
	
	
	public Music getCurrentMusic() {
		return this.currentMusic;
	}
	
	
	public long getStartTime() {
		return this.startTime;
	}
	
	
	public long getOffset() {
		return Calendar.getInstance().getTimeInMillis() - this.startTime;
	}
	
	
	// ===== Setters =====
	
	
	public void setCurrentMusic(Music currentMusic) {
		this.currentMusic = currentMusic;
	}
	
	
	public void setStartTime(long startTime) {
		this.startTime = startTime;
	}
	
}
