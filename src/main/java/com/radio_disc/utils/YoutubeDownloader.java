package com.radio_disc.utils;

import java.io.File;
import java.io.IOException;

import com.github.kiulian.downloader.YoutubeException;
import com.github.kiulian.downloader.model.YoutubeVideo;
import com.github.kiulian.downloader.model.formats.AudioVideoFormat;
import com.radio_disc.models.Music;
import com.radio_disc.models.Proposition;

import ws.schild.jave.Encoder;
import ws.schild.jave.EncoderException;
import ws.schild.jave.MultimediaObject;
import ws.schild.jave.encode.AudioAttributes;
import ws.schild.jave.encode.EncodingAttributes;

/**
 * This class is used to download a youtube video and convert it to mp3
 * 
 * @author Hugo Guerrier
 */
public class YoutubeDownloader {
	
	
	// ===== Attributes =====
	
	
	private Proposition proposition;
	
	
	// ===== Constructors =====
	
	
	public YoutubeDownloader(Proposition prop) {
		this.proposition = prop;
	}
	
	
	// ===== Getters =====
	
	
	public Proposition getProposition() {
		return this.proposition;
	}
	
	
	// ===== Setters =====
	
	
	public void setProposition(Proposition proposition) {
		this.proposition = proposition;
	}
	
	
	// ===== Class methods =====
	
	
	/**
	 * Download the music and return the music object
	 * 
	 * @return The music object result
	 */
	public Music download() throws YoutubeException, IOException, EncoderException {
		// Get and configure the youtube downloader
		com.github.kiulian.downloader.YoutubeDownloader downloader = new com.github.kiulian.downloader.YoutubeDownloader();
		downloader.setParserRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36");
		downloader.setParserRetryOnFailure(1);
		
		// Get the youtube video from the id and it audio info
		YoutubeVideo video = downloader.getVideo(this.proposition.getVideoId());
		AudioVideoFormat videoFormat = video.videoWithAudioFormats().get(0);
		
		// Download the video with the audio
		File videoFile = video.download(videoFormat, Config.videoDir);
		
		// Convert the video file to an audio file
		String audioFileName = Config.videoDir.getAbsoluteFile() + "/" + this.proposition.getVideoId() + ".ogg";
		File audioFile = new File(audioFileName);
		
		// Set the audio export attributes
		AudioAttributes audioAttributes = new AudioAttributes();
		audioAttributes.setCodec("libvorbis");
		audioAttributes.setBitRate(videoFormat.bitrate());
		audioAttributes.setSamplingRate(videoFormat.audioSampleRate());
		audioAttributes.setChannels(2);
		
		// Set the audio encoding attributes
		EncodingAttributes encodingAttributes = new EncodingAttributes();
		encodingAttributes.setOutputFormat("ogg");
		encodingAttributes.setAudioAttributes(audioAttributes);
		
		// Encode the new audio
		Encoder encoder = new Encoder();
		encoder.encode(new MultimediaObject(videoFile), audioFile, encodingAttributes);
		
		// Remove the video file
		videoFile.delete();
		
		// Create the music object
		Music res = new Music();
		res.setFile(audioFile);
		
		// Return the result
		return res;
	}
	
}
