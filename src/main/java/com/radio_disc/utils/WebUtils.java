package com.radio_disc.utils;

import org.json.simple.JSONObject;

/**
 * Contains practical tools for Web communication
 * 
 * @author Emilie Siau
 */
public class WebUtils {
	
	
	/**
	 * Gives the default success JSON response : {success: true}
	 * 
	 * @return the JSONObject response
	 */
	@SuppressWarnings("unchecked")
	public static JSONObject getDefaultSuccessResponse() {
		JSONObject res = new JSONObject();
		res.put("success", true);
		return res;
	}
	
	/**
	 * Gives the default fail JSON response : {success: false, errorMessage: "message"}
	 * 
	 * @param message The error message
	 * @return The JSONObject response
	 */
	@SuppressWarnings("unchecked")
	public static JSONObject getDefaultFailResponse(String message) {
		JSONObject res = new JSONObject();
		res.put("success", false);
		res.put("errorMessage", message);
		return res;
	}

}
