package com.radio_disc.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.security.auth.login.LoginException;

import com.radio_disc.exceptions.DiscordException;

import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.audio.AudioSendHandler;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.events.GenericEvent;
import net.dv8tion.jda.api.events.ReadyEvent;
import net.dv8tion.jda.api.events.guild.GuildJoinEvent;
import net.dv8tion.jda.api.hooks.EventListener;
import net.dv8tion.jda.api.requests.GatewayIntent;

/**
 * This class handle the music sending to the discord bot
 * 
 * @author Hugo Guerrier
 */
public class DiscordBot {
	
	// ===== Attributes =====
	
	
	private String key;
	private JDA jda;
	private Map<Guild, AudioSendHandler> sendHandlerMap;
	
	private static DiscordBot instance = null;
	
	
	// ===== Constructors =====
	
	
	private DiscordBot(String key) throws DiscordException {
		this.key = key;
		
		// Create the JDA
		try {			
			this.jda = JDABuilder.createDefault(this.key)
					.disableIntents(GatewayIntent.GUILD_MESSAGE_TYPING,
							GatewayIntent.GUILD_MESSAGE_REACTIONS,
							GatewayIntent.GUILD_PRESENCES,
							GatewayIntent.GUILD_MESSAGES,
							GatewayIntent.GUILD_EMOJIS)
					.build();
		} catch (LoginException e) {
			
			Logger.log("Cannot connect to the discord service", Logger.ERROR);
			Logger.log(e, Logger.ERROR);
			throw new DiscordException("Cannot connect to the discord bot, check error log for more details");
			
		}
	}
	
	public static DiscordBot getInstance() throws DiscordException {
		if(DiscordBot.instance == null) {
			DiscordBot.instance = new DiscordBot(Config.discordAPIKey);
		}
		return DiscordBot.instance;
	}
	
	
	// ===== Getters =====
	
	
	public AudioSendHandler getAudioSendHandler(Guild guild) {
		return this.sendHandlerMap.get(guild);
	}
	
	
	public Map<Guild, AudioSendHandler> getHandlersMap() {
		return this.sendHandlerMap;
	}
	
	
	// ===== Setters =====
	
	
	public void putGuild(Guild guild, AudioSendHandler handler) {
		this.sendHandlerMap.put(guild, handler);
	}
	
	
	public void removeGuild(Guild guild) {
		this.sendHandlerMap.remove(guild);
	}
	
	
	// ===== Class methods =====
	
	
	/**
	 * Initialize a guild and its handler in the audio handlers map
	 * 
	 * @param guild The guild to initialize
	 */
	public void initGuild(Guild guild) {
		if(!this.sendHandlerMap.containsKey(guild)) {
			// TODO : Add the guild to the bot and create an audio send handler
		}
	}
	
	
	// ===== Bot control methods =====
	
	
	/**
	 * Resume the music playing
	 */
	public void resume() {
		
	}
	
	
	/**
	 * Pause the music playing
	 */
	public void pause() {
		
	}
	
	
	/**
	 * Start the discord bot by start playing music on every server
	 */
	public void start() {
		
	}
	
	
	/**
	 * Stop the discord bot and stop all music playing
	 */
	public void stop() {
		
	}
	
	
	// ===== Internal classes =====
	
	
	private class ReadyListener implements EventListener {
		
		// ===== Attributes =====
		
		private DiscordBot bot;
		
		// ===== Constructor =====
		
		public ReadyListener(DiscordBot bot) {
			this.bot = bot;
		}
		
		// ===== Class methods =====
		
		@Override
		public void onEvent(GenericEvent event) {
			if(event instanceof ReadyEvent) {
				// Get all the guilds and add them to the bot guild list
				for(Guild guild : event.getJDA().getGuilds()) {
					this.bot.initGuild(guild);
				}
			}
		}

	}
	
	
	private class GuildJoinListener implements EventListener {

		// ===== Attributes =====
		
		private DiscordBot bot;
		
		// ===== Constructor =====
		
		public GuildJoinListener(DiscordBot bot) {
			this.bot = bot;
		}
		
		// ===== Class methods =====
		
		@Override
		public void onEvent(GenericEvent event) {
			if(event instanceof GuildJoinEvent) {
				this.bot.initGuild(((GuildJoinEvent) event).getGuild());
			}
		}
		
	}
	
}
