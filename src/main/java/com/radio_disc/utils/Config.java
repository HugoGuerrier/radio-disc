package com.radio_disc.utils;

import java.io.File;

/**
 * This class contains all the application configuration
 * 
 * @author Hugo Guerrier
 */
public class Config {
	
	// ===== Config constants =====
	
	
	public static final int DEV_ENV = 0;
	public static final int PROD_ENV = 1;
	
	
	// ===== Config attributes =====
	
	
	/** The base path of the application */
	public static String basePath = null;
	
	/** The folder where videos are downloaded */
	public static File videoDir = null;
	
	/** The mongoDB database name */
	public static String databaseName = "radio_disc";
	
	/** The youtube API key to access the youtube database */
	public static String youtubeAPIKey = "AIzaSyBt28Uc2yN35VVC4O8bdzlhyWCtwBlu_0c";
	
	/** The discord API key to control the bot */
	public static String discordAPIKey = "ODMzOTc0MjM5MzgyOTk0OTU1.YH6JCQ.t1KXmLBIITsa9KSZFiGX9f6sZkw";
	
	/** If the application is in the verbose mode */
	public static boolean verbose = false;
	
	/** The execution environment */
	public static int env = Config.PROD_ENV;
	
	
	// ===== Config methods =====
	
	
	public static void display() {
		StringBuilder messageBuilder = new StringBuilder();
		messageBuilder.append("Server configuration :\n");
		
		messageBuilder.append("\tbasePath = " + Config.basePath + "\n");
		
		messageBuilder.append("\tvideoDir = " + Config.videoDir.getAbsolutePath() + "\n");
		
		messageBuilder.append("\tdatabaseName = " + Config.databaseName + "\n");
		
		messageBuilder.append("\tverbose = ");
		if(Config.verbose) messageBuilder.append("true\n");
		else messageBuilder.append("false\n");
		
		messageBuilder.append("\tenv = " + String.valueOf(Config.env));
		
		Logger.log(messageBuilder.toString(), Logger.INFO);
	}

}
