package com.radio_disc.models;

import org.bson.Document;
import org.json.simple.JSONObject;

/**
 * This class represent a music proposition
 * 
 * @author Hugo Guerrier
 */
public class Proposition extends Documentable implements Jsonable {

	// ===== Attributes =====
	
	
	private String videoId;
	private String title;
	private String channel;
	private int duration;
	private long score;
	private boolean isMusic;
	
	
	// ===== Constructors =====
	
	
	/**
	 * Create a music proposition with the wanted parameters
	 * 
	 * @param id The youtube video id
	 * @param title The title of the youtube video
	 * @param channel The channel that post the video
	 * @param duration The video duration
	 * @param score The proposition score
	 */
	public Proposition(String id, String title, String channel, int duration, long score) {
		this.videoId = id;
		this.title = title;
		this.channel = channel;
		this.duration = duration;
		this.score = score;
		this.isMusic = false;
	}
	
	
	/**
	 * Create a music proposition with the default parameters
	 */
	public Proposition() {
		this(null, null, null, -1, 0);
	}
	
	
	// ===== Getters =====
	
	
	public String getVideoId() {
		return this.videoId;
	}
	
	
	public String getTitle() {
		return this.title;
	}
	
	
	public String getChannel() {
		return this.channel;
	}
	
	
	public int getDuration() {
		return this.duration;
	}
	
	
	public long getScore() {
		return this.score;
	}
	
	
	public boolean isMusic() {
		return this.isMusic;
	}
	
	
	// ===== Setters =====
	
	
	public void setVideoId(String videoId) {
		this.videoId = videoId;
	}
	
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	
	public void setChannel(String channel) {
		this.channel = channel;
	}
	
	
	public void setDuration(int duration) {
		this.duration = duration;
	}
	
	
	/**
	 * Set the duration from the youtube duration format
	 * 
	 * @param durationYT The duration formated in the Youtube format
	 */
	public void setDurationYoutubeFormat(String durationYT) {
		// Remove the PT at the start
		durationYT = durationYT.substring(2);
		
		// Prepare the result
		int time = 0;
		int start = 0;
		
		// Iterate over the string and for each letter, get the time before it
		for(int i = 0 ; i < durationYT.length() ; i++) {
			char curChar = durationYT.charAt(i);
			
			// If this is the hour tag
			if(curChar == 'H') {
				time += (Integer.valueOf(durationYT.substring(start, i))) * 3600;
				start = i + 1;
			}
			
			// If this is the minute tag
			else if(curChar == 'M') {
				time += (Integer.valueOf(durationYT.substring(start, i))) * 60;
				start = i + 1;
			} 
			
			// If this is the second tag
			else if(curChar == 'S') {
				time += Integer.valueOf(durationYT.substring(start, i));
				start = i + 1;
			}
		}
		
		// Set the time
		this.duration = time;
	}
	
	
	public void setScore(long score) {
		this.score = score;
	}
	
	
	public void setMusic(boolean isMusic) {
		this.isMusic = isMusic;
	}
	
	
	// ===== Class methods =====

	
	@Override
	@SuppressWarnings("unchecked")
	public JSONObject toJson() {
		JSONObject res = new JSONObject();
		
		res.put(Documentable.ID_FIELD, this.id);
		res.put("videoId", this.videoId);
		res.put("title", this.title);
		res.put("channel", this.channel);
		res.put("duration", (long) this.duration);
		res.put("score", this.score);
		
		return res;
	}

	
	@Override
	public void fromJson(JSONObject json) {
		this.id = (Long) json.get(Documentable.ID_FIELD);
		this.videoId = (String) json.get("videoId");
		this.title = (String) json.get("title");
		this.channel = (String) json.get("channel");
		Long duration = (Long) json.get("duration");
		this.duration = duration.intValue();
		this.score = (Long) json.get("score");
	}
	
	
	@Override
	public Document toDocument() {
		Document res = new Document();
		
		res.append(Documentable.ID_FIELD, this.id);
		res.append("videoId", this.videoId);
		res.append("title", this.title);
		res.append("channel", this.channel);
		res.append("duration", (long) this.duration);
		res.append("score", this.score);
		
		return res;
	}
	
	
	@Override
	public void fromDocument(Document doc) {
		this.id = (Long) doc.get(Documentable.ID_FIELD);
		this.videoId = (String) doc.get("videoId");
		this.title = (String) doc.get("title");
		this.channel = (String) doc.get("channel");
		Long duration = (Long) doc.get("duration");
		this.duration = duration.intValue();
		this.score = (Long) doc.get("score");
	}
	
	
	// ===== Standard methods =====
	
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof Proposition && obj != null) {
			Proposition p = (Proposition) obj;
			return (this.videoId.equals(p.getVideoId()) &&
					this.title.equals(p.getTitle()) &&
					this.channel.equals(p.getChannel()) &&
					this.duration == p.getDuration());
		}
		return false;
	}
	
	
	@Override
	public String toString() {
		return "Proposition(" + this.videoId + ", " + this.title + ", " + this.channel + ", " + this.duration + ", " + this.score + ")";
	}
	
}
