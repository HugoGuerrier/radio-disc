package com.radio_disc.models;

import org.bson.Document;
import org.json.simple.JSONObject;

/**
 * This class represent an administrator
 * 
 * @author Hugo Guerrier
 */
public class Admin extends Documentable implements Jsonable {
	
	// ===== Attributes =====
	
	
	private String pseudo;
	private String mail;
	private String password;
	private int level;
	
	
	// ===== Constructors =====
	
	
	/**
	 * Create an admin with the wanted parameters
	 * 
	 * @param pseudo The admin's pseudo
	 * @param mail The admin's mail
	 * @param password The admin's password in sha512
	 * @param level The admin persmission level
	 */
	public Admin(String pseudo, String mail, String password, int level) {
		this.pseudo = pseudo;
		this.mail = mail;
		this.password = password;
		this.level = level;
	}
	
	
	/**
	 * Create an admin with the default parameters
	 */
	public Admin() {
		this(null, null, null, -1);
	}
	
	
	// ===== Getters =====
	
	
	public String getPseudo() {
		return this.pseudo;
	}
	
	
	public String getMail() {
		return this.mail;
	}
	
	
	public String getPassword() {
		return this.password;
	}
	
	
	public int getLevel() {
		return this.level;
	}
	
	
	// ===== Setters =====
	
	
	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}
	
	
	public void setMail(String mail) {
		this.mail = mail;
	}
	
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	
	public void setLevel(int level) {
		this.level = level;
	}
	
	
	// ===== Class methods =====

	
	@Override
	@SuppressWarnings("unchecked")
	public JSONObject toJson() {
		JSONObject res = new JSONObject();
		
		res.put(Documentable.ID_FIELD, this.id);
		res.put("pseudo", this.pseudo);
		res.put("mail", this.mail);
		res.put("password", this.password);
		res.put("level", (long) this.level);
		
		return res;
	}

	
	@Override
	public void fromJson(JSONObject json) {
		this.id = (Long) json.get(Documentable.ID_FIELD);
		this.pseudo = (String) json.get("pseudo");
		this.mail = (String) json.get("mail");
		this.password = (String) json.get("password");
		Long level = (Long) json.get("level");
		this.level = level.intValue();
	}
	
	
	@Override
	public Document toDocument() {
		Document res = new Document();
		
		res.append(Documentable.ID_FIELD, this.id);
		res.append("pseudo", this.pseudo);
		res.append("mail", this.mail);
		res.append("password", this.password);
		res.append("level", (long) this.level);
		
		return res;
	}
	
	
	@Override
	public void fromDocument(Document doc) {
		this.id = (Long) doc.get(Documentable.ID_FIELD);
		this.pseudo = (String) doc.get("pseudo");
		this.mail = (String) doc.get("mail");
		this.password = (String) doc.get("password");
		Long level = (Long) doc.get("level");
		this.level = level.intValue();
	}
	
	
	// ===== Standard methods =====
	
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof Admin && obj != null) {
			Admin a = (Admin) obj;
			return (this.pseudo.equals(a.getPseudo()) && 
					this.mail.equals(a.getMail()) && 
					this.password.equals(a.getPassword()) && 
					this.level == a.getLevel());
		}
		return false;
	}
	
	
	@Override
	public String toString() {
		return "Admin(" + this.pseudo + ", " + this.mail + ", " + this.password + ", " + this.level + ")";
	}

}
