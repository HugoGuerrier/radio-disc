package com.radio_disc.models;

import org.bson.Document;

/**
 * This abstract class represent an object that can be input in the database
 * 
 * @author Hugo Guerrier
 */
public abstract class Documentable {
	
	// ===== Attributes =====
	
	
	public static final String ID_FIELD = "id";
	protected long id = -1;
	
	
	// ===== Getters =====
	
	
	public long getId() {
		return this.id;
	}
	
	
	// ===== Setters =====
	
	
	public void setId(long id) {
		this.id = id;
	}
	
	
	// ===== Class methods =====
	
	
	/**
	 * Get the Bson document of the current object
	 * 
	 * @return The Bson document
	 */
	public abstract Document toDocument();
	
	
	/**
	 * Configure the current object from the Bson document
	 * 
	 * @param object The Bson document
	 */
	public abstract void fromDocument(Document doc);
	
}
