package com.radio_disc.models;

import org.json.simple.JSONObject;

/**
 * Offer the interface to create the class from JSON and export it to JSON
 * 
 * @author Hugo Guerrier
 */
public interface Jsonable {
	/**
	 * Get the JSONObject from the current object
	 * 
	 * @return The JSONObject
	 */
	public JSONObject toJson();
	
	/**
	 * Configure the current object with the specified JSON
	 * 
	 * @param json The JSON to configure the object
	 */
	public void fromJson(JSONObject json);
}
