package com.radio_disc.models;

import java.io.File;

import org.bson.Document;
import org.json.simple.JSONObject;

/**
 * This class represent a music file that is going to be played
 * 
 * @author Hugo Guerrier
 */
public class Music extends Documentable implements Jsonable, Comparable {

	// ===== Attributes =====
	
	
	private String songName;
	private String artist;
	private String videoId;
	private int duration;
	private File file;
	private long insertDate;
	
	
	// ===== Constructor =====
	
	
	/**
	 * Create a music with the wanted parameters
	 * 
	 * @param songName The name of the music
	 * @param artist The artist
	 * @param duration The duration of the music in seconds (rounded up)
	 * @param file The file of the music
	 */
	public Music(String songName, String artist, String videoId, int duration, File file) {
		this.songName = songName;
		this.artist = artist;
		this.videoId = videoId;
		this.duration = duration;
		this.file = file;
		this.insertDate = -1;
	}
	
	
	/**
	 * Create a music with the default parameters
	 */
	public Music() {
		this(null, null, null, -1, null);
	}
	
	
	// ===== Getters =====
	
	
	public String getSongName() {
		return this.songName;
	}
	
	
	public String getArtist() {
		return this.artist;
	}
	
	
	public String getVideoId() {
		return this.videoId;
	}
	
	
	public int getDuration() {
		return this.duration;
	}
	
	
	public File getFile() {
		return this.file;
	}
	
	
	public long getInsertDate() {
		return this.insertDate;
	}
	
	
	// ===== Setters =====
	
	
	public void setSongName(String songName) {
		this.songName = songName;
	}
	
	
	public void setArtist(String artist) {
		this.artist = artist;
	}
	
	
	public void setVideoId(String videoId) {
		this.videoId = videoId;
	}
	
	
	public void setDuration(int duration) {
		this.duration = duration;
	}
	
	
	public void setFile(File file) {
		this.file = file;
	}
	
	
	public void setInsertDate(long insertDate) {
		this.insertDate = insertDate;
	}
	
	
	// ===== Class methods =====
	
	
	@Override
	@SuppressWarnings("unchecked")
	public JSONObject toJson() {
		JSONObject res = new JSONObject();
		
		res.put(Documentable.ID_FIELD, this.id);
		res.put("songName", this.songName);
		res.put("artist", this.artist);
		res.put("videoId", this.videoId);
		res.put("duration", (long) this.duration);
		res.put("file", this.file.getAbsolutePath());
		res.put("insertDate", this.insertDate);
		
		return res;
	}

	
	@Override
	public void fromJson(JSONObject json) {
		this.id = (Long) json.get(Documentable.ID_FIELD);
		this.songName = (String) json.get("songName");
		this.artist = (String) json.get("artist");
		this.videoId = (String) json.get("videoId");
		Long duration = (Long) json.get("duration");
		this.duration = duration.intValue();
		this.file = new File((String) json.get("file"));
		this.insertDate = (Long) json.get("insertDate");
	}
	
	
	@Override
	public Document toDocument() {
		Document res = new Document();
		
		res.append(Documentable.ID_FIELD, this.id);
		res.append("songName", this.songName);
		res.append("artist", this.artist);
		res.append("videoId", this.videoId);
		res.append("duration", (long) this.duration);
		res.append("file", this.file.getAbsolutePath());
		res.append("insertDate", this.insertDate);
		
		return res;
	}
	
	
	@Override
	public void fromDocument(Document doc) {
		this.id = (Long) doc.get(Documentable.ID_FIELD);
		this.songName = (String) doc.get("songName");
		this.artist = (String) doc.get("artist");
		this.videoId = (String) doc.get("videoId");
		Long duration = (Long) doc.get("duration");
		this.duration = duration.intValue();
		this.file = new File((String) doc.get("file"));
		this.insertDate = (Long) doc.get("insertDate");
	}
	
	
	// ===== Standard methods =====
	
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof Music && obj != null) {
			Music m = (Music) obj;
			return (this.songName.equals(m.getSongName()) &&
					this.videoId.equals(m.getVideoId()) &&
					this.artist.equals(m.getArtist()) &&
					this.duration == m.getDuration());
		}
		return false;
	}
	
	
	@Override
	public String toString() {
		return "Music(" + this.songName + ", " + this.artist + ", " + this.duration + ", " + this.file.getAbsoluteFile() + ")";
	}


	@Override
	public int compareTo(Object o) {
		if(o instanceof Music && o != null) {
			Music m = (Music) o;
			return ((Long) this.insertDate).compareTo(m.getInsertDate());
		}
		return 1;
	}
	
}
