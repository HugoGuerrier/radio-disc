package com.radio_disc.exceptions;

public class YoutubeException extends Exception {

	private static final long serialVersionUID = -8697109219938770324L;

	public YoutubeException(String msg) {
		super(msg);
	}
	
}
