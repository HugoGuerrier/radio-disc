package com.radio_disc.exceptions;

public class DatabaseException extends Exception {

	private static final long serialVersionUID = -548844798271350615L;
	
	public DatabaseException(String msg) {
		super(msg);
	}

}
