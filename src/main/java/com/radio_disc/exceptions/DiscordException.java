package com.radio_disc.exceptions;

public class DiscordException extends Exception {

	private static final long serialVersionUID = 228028649907039037L;
	
	public DiscordException(String msg) {
		super(msg);
	}

}
