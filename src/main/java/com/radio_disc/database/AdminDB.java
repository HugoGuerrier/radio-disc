package com.radio_disc.database;

import org.bson.Document;

import com.radio_disc.exceptions.DatabaseException;
import com.radio_disc.models.Admin;

/**
 * This is a simple class with static methods to handle the administration database
 * 
 * @author Hugo Guerrier
 */
public class AdminDB {
	
	// ===== Attributes =====
	
	
	private static final String collectionName = "admins";
	
	
	// ===== Class methods =====
	
	
	/**
	 * Get an admin by its pseudo
	 * 
	 * @param pseudo The pseudo of the admin
	 * @return The admin with the wanted pseudo, null if it doesn't exists
	 */
	public static Admin getAdminByPseudo(String pseudo) {
		// Get the result document
		Database db = Database.getInstance();
		Document adminDoc = db.getOne(AdminDB.collectionName, "pseudo", pseudo);
		
		// Prepare the result
		Admin res = null;
		
		if(adminDoc != null) {
			// Create an admin object
			res = new Admin();
			res.fromDocument(adminDoc);
		}
		
		// Return the result
		return res;
	}
	
	
	/**
	 * Get an admin by its mail
	 * 
	 * @param mail The mail of the admin
	 * @return The admin with the wanted mail, null if it doesn't exists
	 */
	public static Admin getAdminByMail(String mail) {
		// Get the result document
		Database db = Database.getInstance();
		Document adminDoc = db.getOne(AdminDB.collectionName, "mail", mail);
		
		// Prepare the result
		Admin res = null;
		
		if(adminDoc != null) {
			// Create an admin object
			res = new Admin();
			res.fromDocument(adminDoc);
		}
		
		// Return the result
		return res;
	}
	
	
	/**
	 * Add an admin to the database
	 * 
	 * @param admin The admin you want to add
	 */
	public synchronized static void addAdmin(Admin admin) throws DatabaseException {
		// Verify the admin uniqueness
		if(AdminDB.getAdminByMail(admin.getMail()) != null || AdminDB.getAdminByPseudo(admin.getPseudo()) != null) {
			throw new DatabaseException("Admin already present in the database : " + admin.toString());
		} 
		
		// Insert the administrator
		Database db = Database.getInstance();
		db.insert(AdminDB.collectionName, admin);
	}
	
	
	/**
	 * Delete an admin from the database
	 * 
	 * @param admin The admin you want to delete
	 * @return True if the admin were deleted, false otherwise
	 */
	public static boolean deleteAdmin(Admin admin) {
		// Remove the admin
		Database db = Database.getInstance();
		return db.delete(AdminDB.collectionName, admin);
	}
	
	
	/**
	 * Update an admin to a new admin
	 * 
	 * @param oldAdmin The admin to update
	 * @param newAdmin The new admin state
	 * @return True if the admin were updated, false otherwise
	 */
	public static boolean updateAdmin(Admin oldAdmin, Admin newAdmin) throws DatabaseException {
		// Update the admin
		Database db = Database.getInstance();
		return db.update(AdminDB.collectionName, oldAdmin, newAdmin);
	}
	
}
