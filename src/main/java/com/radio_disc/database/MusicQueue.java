package com.radio_disc.database;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.bson.Document;

import com.mongodb.client.FindIterable;
import com.radio_disc.exceptions.DatabaseException;
import com.radio_disc.models.Music;

/**
 * This class handle all changes in the music queue
 * 
 * @author Hugo Guerrier
 * @author Emilie Siau
 */
public class MusicQueue {
	
	// ===== Attributes =====
	
	
	private static final String collectionName = "queue";
	
	
	// ===== Class methods =====
	
	
	/**
	 * Get the full ordered music queue
	 * 
	 * @return The full ordered queue
	 */
	public static List<Music> getQueue() {
		// Prepare the result
		List<Music> res = new ArrayList<>();
		
		// Get the database and the iterable
		Database db = Database.getInstance();
		FindIterable<Document> dbResult = db.getMany(MusicQueue.collectionName);
		
		// Create the list
		for(Document doc : dbResult) {
			Music tmp = new Music();
			tmp.fromDocument(doc);
			res.add(tmp);
		}
		
		// Sort the result
		res.sort(null);
		
		// Return the result
		return res;
	}
	
	
	/**
	 * Get the next music in the queue and remove it from the database
	 * 
	 * @return The next music in the queue, null if the queue is empty
	 */
	public static Music pop() {
		// Get the database instance
		Database db = Database.getInstance();
		Document dbResult = db.getExtremum(MusicQueue.collectionName, "insertDate", false);
		
		// Prepare the result
		Music res = null;
		
		// If a music were found
		if(dbResult != null) {
			res = new Music();
			res.fromDocument(dbResult);
			
			// Remove the first music
			MusicQueue.deleteMusic(res);
		}
		
		// Return the result
		return res;
	}
	
	
	/**
	 * Get a video in the queue by its ID
	 * 
	 * @param videoId The video ID
	 * @return The video in the queue or null if it doesn't exists
	 */
	public static Music getMusicByVideoId(String videoId) {
		// Get the database
		Database db = Database.getInstance();
		
		// Get the Music document
		Document doc = db.getOne(MusicQueue.collectionName, "videoId", videoId);
		
		// Prepare the result
		Music res = null;
		
		// If music were found
		if(doc != null) {
			res = new Music();
			res.fromDocument(doc);
		}
		
		return res;
	}
	
	
	/**
	 * Add a music to the queue
	 * 
	 * @param music The music to add
	 */
	public static void addMusic(Music music) throws DatabaseException {
		if(music.getInsertDate() != -1) {
			throw new DatabaseException("The music have already benn inserted in the database");
		}
		
		// Get the database
		Database db = Database.getInstance();
		
		// Get the current epoch in milliseconds
		Calendar cal = Calendar.getInstance();
		music.setInsertDate(cal.getTimeInMillis());
		
		// Insert it in the database
		db.insert(MusicQueue.collectionName, music);
	}
	

	/**
	 * Delete a music from the queue
	 * 
	 * @param music The music to remove
	 * @return If the music was deleted
	 */
	public static boolean deleteMusic(Music music) {
		// Get the database instance and remove the music
		Database db = Database.getInstance();
		return db.delete(MusicQueue.collectionName, music);
	}
	
}
