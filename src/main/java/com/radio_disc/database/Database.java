package com.radio_disc.database;

import org.bson.Document;

import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.radio_disc.exceptions.DatabaseException;
import com.radio_disc.models.Documentable;
import com.radio_disc.utils.Config;

/**
 * This class contains all methods to manage the database
 * 
 * @author Hugo Guerrier
 */
public class Database {

	// ===== Attributes =====
	
	
	private String databaseName;
	private MongoClient client;
	private MongoDatabase database;
	
	private static Database instance = null;
	
	
	// ===== Constructors =====
	
	
	/**
	 * Create a mongoDB connection with the specified database
	 * 
	 * @param name The name of the database
	 */
	private Database(String name) {
		this.databaseName = name;
		this.client = MongoClients.create();
		this.database = this.client.getDatabase(this.databaseName);
	}
	
	
	/**
	 * The get instance method of this singleton class
	 * 
	 * @return The unique database instance
	 */
	public static Database getInstance() {
		// Test if the instance is null
		if(Database.instance == null) {
			Database.instance = new Database(Config.databaseName);
		}
		
		// Return the database instance
		return Database.instance;
	}
	
	
	// ===== Getters =====
	
	
	public String getDatabaseName() {
		return this.databaseName;
	}
	
	
	public MongoDatabase getDatabase() {
		return this.database;
	}
	
	
	// ===== Class methods =====
	
	
	/**
	 * Get the next id for the wanted collection
	 * 
	 * @param collectionName The collection name
	 * @return The next id
	 */
	private long getNextId(String collectionName) {
		// Get the collection
		MongoCollection<Document> collection = this.database.getCollection(collectionName);
		
		// Prepare the filters
		Document sortDoc = new Document(Documentable.ID_FIELD, -1);
		Document maxIdDoc = collection.find().sort(sortDoc).limit(1).first();
		
		// Return the identifier
		return maxIdDoc != null ? maxIdDoc.getLong(Documentable.ID_FIELD) + 1 : 0;
	}
	
	
	/**
	 * Get one document from a key and a value in a collection
	 * 
	 * @param collectionName The collection
	 * @param field The key
	 * @param value The value
	 * @return The first result to match the key and the value
	 */
	public Document getOne(String collectionName, String field, Object value) {
		// Prepare the filter and the collection
		MongoCollection<Document> collection = this.database.getCollection(collectionName);
		Document filterDoc = new Document(field, value);
		
		// Get the result
		return collection.find(filterDoc).first();
	}
	
	
	/**
	 * Get the maximum or minimum in a collection on a certain field
	 * 
	 * @param collectionName The collection to look in
	 * @param field The field to sort on
	 * @param max If you want the maximum, else it gives you the minimum
	 * @return The document if it exists, null else
	 */
	public Document getExtremum(String collectionName, String field, boolean max) {
		// Prepare the filter and get the collection
		MongoCollection<Document> collection = this.database.getCollection(collectionName);
		Document sortDoc = new Document(field, (max ? -1 : 1));
		
		// Return the result
		return collection.find().sort(sortDoc).limit(1).first();
	}
	
	
	/**
	 * Get an iterator of document of a collection with the wanted key / value pair
	 * 
	 * @param collectionName The collection
	 * @param field The key
	 * @param value The value
	 * @return The iterator of all results
	 */
	public FindIterable<Document> getMany(String collectionName, String field, Object value) {
		// Prepare the filter and the collection
		MongoCollection<Document> collection = this.database.getCollection(collectionName);
		Document filterDoc = new Document(field, value);
		
		// Return the result
		return collection.find(filterDoc);
	}
	
	
	/**
	 * Get all the collection
	 * 
	 * @param collectionName The collection name you want to get
	 * @return The iterable containing all the collection
	 */
	public FindIterable<Document> getMany(String collectionName) {
		// Prepare the collection
		MongoCollection<Document> collection = this.database.getCollection(collectionName);
		
		// Return the result
		return collection.find();
	}
	
	
	/**
	 * Insert a object in the database
	 * 
	 * @param collectionName The collection to insert the object in
	 * @param object The object to insert
	 * @return If the insertion has worked
	 */
	public synchronized void insert(String collectionName, Documentable object) throws DatabaseException {
		// Verify the object id
		if(object.getId() != -1) {
			throw new DatabaseException("Cannot insert a document with a positive id, it probably already exists");	
		}
		
		// Get the next collection id
		object.setId(this.getNextId(collectionName));
		
		// Insert the object in the database
		MongoCollection<Document> collection = this.database.getCollection(collectionName);
		collection.insertOne(object.toDocument());
	}
	
	
	/**
	 * Delete the wanted document from the collection
	 * 
	 * @param collectionName The collection name
	 * @param doc The document to delete
	 * @return If the deletion has worked
	 */
	public boolean delete(String collectionName, Documentable doc) {
		// Get the collection and prepare the filter
		MongoCollection<Document> collection = this.database.getCollection(collectionName);
		Document filterDoc = new Document(Documentable.ID_FIELD, doc.getId());
		
		// Delete the document
		return collection.deleteOne(filterDoc).getDeletedCount() == 1;
	}
	
	
	/**
	 * Drop a collection in the database
	 * 
	 * @param collectionName The collection to drop
	 */
	public void drop(String collectionName) {
		// Get the collection
		MongoCollection<Document> collection = this.database.getCollection(collectionName);
		
		// Drop the collection
		collection.drop();
	}
	
	
	/**
	 * Update the old document by its newer version
	 * 
	 * @param collectionName The collection name
	 * @param oldDoc The old doc to update
	 * @param newDoc The new version of the doc
	 * @return If the update has affected a document
	 */
	public boolean update(String collectionName, Documentable oldDoc, Documentable newDoc) throws DatabaseException {
		// Verify the old document id
		if(oldDoc.getId() < 0) {
			throw new DatabaseException("Cannot update a document with a negative id");
		}
		
		// Get the collection and prepare the filter
		MongoCollection<Document> collection = this.database.getCollection(collectionName);
		Document filterDoc = new Document(Documentable.ID_FIELD, oldDoc.getId());
		
		// Prepare the update
		Document newDocDoc = newDoc.toDocument();
		newDocDoc.remove(Documentable.ID_FIELD);
		Document updateDoc = new Document("$set", newDocDoc);
		
		// Update the collection
		return collection.updateOne(filterDoc, updateDoc).getModifiedCount() == 1;
	}
	
	
	/**
	 * Add a value to a certain field in a collection
	 * 
	 * @param collectionName The collection name to update
	 * @param doc The document to update
	 * @param field The field to increase
	 * @param value The value to add
	 * @return If the update has worked
	 */
	public boolean increment(String collectionName, Documentable doc, String field, int value) throws DatabaseException {
		// Verify the document id
		if(doc.getId() < 0) {
			throw new DatabaseException("Cannot update a document with a negative id");
		}
		
		// Get the collection and prepare the filter
		MongoCollection<Document> collection = this.database.getCollection(collectionName);
		Document increaseDoc = new Document(field, value);
		Document updateDoc = new Document("$inc", increaseDoc);
		Document filterDoc = new Document(Documentable.ID_FIELD, doc.getId());
		
		// Update the collection
		return collection.updateOne(filterDoc, updateDoc).getModifiedCount() == 1;
	}
	
}
