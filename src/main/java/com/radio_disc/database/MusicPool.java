package com.radio_disc.database;

import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.spi.SyncResolver;

import org.bson.Document;

import com.mongodb.client.FindIterable;
import com.radio_disc.exceptions.DatabaseException;
import com.radio_disc.models.Admin;
import com.radio_disc.models.Documentable;
import com.radio_disc.models.Music;
import com.radio_disc.models.Proposition;

/**
 * This class handle all music pool changes
 * 
 * @author Hugo Guerrier
 */
public class MusicPool {
	
	// ===== Attributes =====
	
	
	private static final String collectionName = "queue";
	
	
	// ===== Class methods =====
	
	
	/**
	 * Get the entire music pool
	 * 
	 * @return The entire music pool
	 */
	public static List<Proposition> getPool() {
		// Prepare the result
		List<Proposition> res = new ArrayList<>();
		
		// Get the iterable in the database
		Database db = Database.getInstance();
		FindIterable<Document> dbResult = db.getMany(MusicPool.collectionName);
		
		// Transform the iterable into list
		for(Document doc : dbResult) {
			Proposition tmp = new Proposition();
			tmp.fromDocument(doc);
			res.add(tmp);
		}
		
		// Return the result
		return res;
	}
	
	
	/**
	 * Get a proposition in the pool by its video id
	 * 
	 * @param videoId The video id
	 * @return The proposition or null if it does not exists
	 */
	public static Proposition getPropositionByVideoId(String videoId) {
		// Get the database and make the request
		Database db = Database.getInstance();
		Document propDoc = db.getOne(MusicPool.collectionName, "videoId", videoId);
		
		// Prepare the result
		Proposition res = null;
		
		if(propDoc != null) {			
			// Create a proposition object
			res = new Proposition();
			res.fromDocument(propDoc);
		}
		
		// Return the result
		return res;
	}
	
	
	/**
	 * Get the proposition with the best score
	 * 
	 * @return The proposition with the highest score
	 */
	public static Proposition getWinner() {
		// Get the database and make the request
		Database db = Database.getInstance();
		Document propDoc = db.getExtremum(MusicPool.collectionName, "score", true);
		
		// Prepare the result
		Proposition res = null;
		
		if(propDoc != null) {			
			// Create a proposition object
			res = new Proposition();
			res.fromDocument(propDoc);
		}
		
		// Return the result
		return res;
	}
	
	
	/**
	 * Add a proposition to the music pool
	 * 
	 * @param proposition The proposition to add to the pool
	 */
	public synchronized static void addProposition(Proposition proposition) throws DatabaseException {
		// Verify that the proposition isn't already in the database
		if(MusicPool.getPropositionByVideoId(proposition.getVideoId()) != null) {
			throw new DatabaseException("Duplicate proposition in the pool : " + proposition.toString());
		}
		
		// Insert the proposition
		Database db = Database.getInstance();
		db.insert(MusicPool.collectionName, proposition);
	}
	
	
	/**
	 * Delete a proposition
	 * 
	 * @param proposition The proposition to delete
	 * @return If the deletion worked
	 */
	public static boolean deleteProposition(Proposition proposition) {
		// Get the database and do the operation
		Database db = Database.getInstance();
		return db.delete(MusicPool.collectionName, proposition);
	}
	
	
	/**
	 * Remove all the propositions from the pool
	 */
	public static void cleanPool() {
		// Get the database and drop the music pool collection
		Database db = Database.getInstance();
		db.drop(MusicPool.collectionName);
	}
	
	
	/**
	 * Vote for the proposition
	 * 
	 * @param proposition The proposition to vote for
	 */
	public static boolean vote(Proposition proposition) throws DatabaseException {
		// Get the proposition in the database
		Database db = Database.getInstance();
		return db.increment(MusicPool.collectionName, proposition, "score", 1);
	}

}
