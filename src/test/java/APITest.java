import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.IOException;

import org.junit.BeforeClass;
import org.junit.Test;

import com.radio_disc.exceptions.YoutubeException;
import com.radio_disc.models.Proposition;
import com.radio_disc.utils.Config;
import com.radio_disc.utils.Logger;
import com.radio_disc.utils.YoutubeAPI;

public class APITest {
	
	// ===== Attributes =====
	
	
	// ===== Configuration methods =====
	
	
	@BeforeClass
	public static void init() {
		// Set the test configuration
		Config.basePath = new File("./src/test/java").getAbsolutePath() + "/";
		Config.videoDir = new File(Config.basePath + "videos");
		Config.databaseName = "radio_disc_test";
		Config.verbose = true;
		Config.env = Config.DEV_ENV;
		
		// Init the application logger
		try {
			Logger.init();
		} catch (IOException e) {
			System.err.println("[ERROR] = Cannot initialize the logger");
		}
		
		// Display the configuration
		Config.display();
	}
	
	
	// ===== Test methods =====
	
	
	@Test
	public void youtube() {
		try {
			
			// Get the youtube API and automatically test the connection
			YoutubeAPI api = new YoutubeAPI(Config.youtubeAPIKey);
			
			// Try getting a video
			Proposition prop = api.getVideoInfo("wy9r2qeouiQ");
			
			assertEquals("wy9r2qeouiQ", prop.getVideoId());
			assertEquals("Carpenter Brut - Turbo Killer", prop.getTitle());
			assertEquals("Carpenter Brut", prop.getChannel());
			assertEquals(209, prop.getDuration());
			assertEquals(0, prop.getScore());
			assertTrue(prop.isMusic());
			
		} catch (YoutubeException e) {
			e.printStackTrace();
			fail();
		}
	}
	
}
