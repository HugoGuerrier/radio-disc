import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.radio_disc.database.AdminDB;
import com.radio_disc.database.Database;
import com.radio_disc.database.MusicPool;
import com.radio_disc.database.MusicQueue;
import com.radio_disc.exceptions.DatabaseException;
import com.radio_disc.models.Admin;
import com.radio_disc.models.Music;
import com.radio_disc.models.Proposition;
import com.radio_disc.utils.Config;
import com.radio_disc.utils.Logger;

public class ModelTest {
	
	// ===== Attributes =====
	

	private final Admin admin1 = new Admin("Admin1", "admin1@mail.com", "NONE", 0);
	private final Admin admin2 = new Admin("Admin2", "admin2@mail.com", "NONE", 1);
	private final Admin admin3 = new Admin("Admin3", "admin3@mail.com", "NONE", 1);
	private final Admin admin1Copy = new Admin("Admin1", "adminCopy@mail.com", "NONE", 0);
	private final Admin admin2Copy = new Admin("AdminCopy", "admin2@mail.com", "NONE", 1);
	
	private final Proposition prop1 = new Proposition("id1", "Title1", "Channel1", 10, 10);
	private final Proposition prop2 = new Proposition("id2", "Title2", "Channel2", 20, 0);
	private final Proposition prop3 = new Proposition("id3", "Title3", "Channel3", 110, 15);
	private final Proposition prop4 = new Proposition("id4", "Title4", "Channel4", 50, 3);
	private final Proposition prop1Copy = new Proposition("id1", "TitleCopy", "ChannelCopy", 0, 0);
	
	private final Music mus1 = new Music("Music1", "Artist1", "vid1", 10, new File("./music1.mp3"));
	private final Music mus2 = new Music("Music2", "Artist2", "vid2", 1, new File("./music2.mp3"));
	private final Music mus3 = new Music("Music3", "Artist3", "vid3", 1000, new File("./music3.mp3"));
	private final Music mus4 = new Music("Music4", "Artist4", "vid4", 52, new File("./music4.mp3"));
	
	// ===== Configuration methods =====
	
	
	@BeforeClass
	public static void init() {
		// Set the test configuration
		Config.basePath = new File("./src/test/java").getAbsolutePath() + "/";
		Config.videoDir = new File(Config.basePath + "videos");
		Config.databaseName = "radio_disc_test";
		Config.verbose = true;
		Config.env = Config.DEV_ENV;
		
		// Init the application logger
		try {
			Logger.init();
		} catch (IOException e) {
			System.err.println("[ERROR] = Cannot initialize the logger");
		}
		
		// Display the configuration
		Config.display();
	}
	
	
	@AfterClass
	public static void clean() {
		// Drop the test database
		Database db = Database.getInstance();
		db.getDatabase().drop();
	}
	
	
	// ===== Test methods =====
	
	
	@Test
	public void admin() {
		// Try inserting base admins
		try {		
			
			AdminDB.addAdmin(admin1);
			AdminDB.addAdmin(admin2);
			AdminDB.addAdmin(admin3);
			
			assertEquals(0, admin1.getId());
			assertEquals(1, admin2.getId());
			assertEquals(2, admin3.getId());
			
		} catch (DatabaseException e) {
			e.printStackTrace();
			fail("Error during the admins insertions");
		}
		
		// Try inserting a double admin with pseudo
		try {
			
			AdminDB.addAdmin(admin1Copy);
			fail("The admin copy should not be inserted");
			
		} catch (DatabaseException e) {
			// Do nothing
		}
		
		// Try inserting a double admin with mail
		try {
			
			AdminDB.addAdmin(admin2Copy);
			fail("The admin copy should not be inserted");
			
		} catch (DatabaseException e) {
			// Do nothing
		}
		
		// Get the admin 1
		Admin res = AdminDB.getAdminByPseudo("Admin1");
		
		assertEquals(0, res.getId());
		assertEquals("admin1@mail.com", res.getMail());
		assertEquals("NONE", res.getPassword());
		assertEquals(0, res.getLevel());
		
		// Delete an admin
		assertTrue(AdminDB.deleteAdmin(admin1));
		
		// Try getting the same admin to verify the deletion
		assertNull(AdminDB.getAdminByPseudo("Admin1"));
		assertNull(AdminDB.getAdminByMail("admin1@mail.com"));
		
		// Update the admin 2 in admin 1
		try {
			
			assertTrue(AdminDB.updateAdmin(admin2, admin1));
			res = AdminDB.getAdminByMail(admin1.getMail());
			
			assertEquals(1, res.getId());
			assertEquals("Admin1", res.getPseudo());
			assertEquals("NONE", res.getPassword());
			assertEquals(0, res.getLevel());
			
		} catch (DatabaseException e) {
			e.printStackTrace();
			fail("Cannot update the admin 2");
		}
	}
	
	
	@Test
	public void pool() {
		// Try inserting the correct propositions
		try {
			
			MusicPool.addProposition(prop1);
			MusicPool.addProposition(prop2);
			MusicPool.addProposition(prop3);
			MusicPool.addProposition(prop4);
			
			assertEquals(0, prop1.getId());
			assertEquals(1, prop2.getId());
			assertEquals(2, prop3.getId());
			assertEquals(3, prop4.getId());
			
		} catch (DatabaseException e) {
			e.printStackTrace();
			fail("Error during the propositions insertions");
		}
		
		// Try inserting the copy
		try {
			
			MusicPool.addProposition(prop1Copy);
			fail("The proposition copy should not be inserted");
			
		} catch (DatabaseException e) {
			// Do nothing
		}
		
		// Test getting all propositions
		List<Proposition> list = MusicPool.getPool();
		assertEquals(4, list.size());
		
		// Test getting the proposition by video id
		Proposition res = MusicPool.getPropositionByVideoId("id2");		
		assertEquals(1, res.getId());
		assertEquals("Title2", res.getTitle());
		assertEquals("Channel2", res.getChannel());
		assertEquals(20, res.getDuration());
		assertEquals(0, res.getScore());
		
		// Test the duration parsing
		res.setDurationYoutubeFormat("PT1H42M56S");
		assertEquals(3600 + (42 * 60) + 56, res.getDuration());
		
		// Delete the proposition 2
		assertTrue(MusicPool.deleteProposition(prop2));
		
		// Try getting the same proposition to confirm deletion
		assertNull(MusicPool.getPropositionByVideoId("id2"));
		
		// Retry the list size
		list = MusicPool.getPool();
		assertEquals(3, list.size());
		
		// Vote for a proposition
		try {
			
			MusicPool.vote(prop1);
			res = MusicPool.getPropositionByVideoId("id1");
			assertEquals(11, res.getScore());
			
		} catch (DatabaseException e) {
			e.printStackTrace();
			fail("Should not happen");
		}
		
		// Try getting the winner
		res = MusicPool.getWinner();
		assertEquals(2, res.getId());
		assertEquals("id3", res.getVideoId());
		assertEquals("Title3", res.getTitle());
		assertEquals("Channel3", res.getChannel());
		assertEquals(15, res.getScore());
		assertEquals(110, res.getDuration());
		
		// Clean the pool
		MusicPool.cleanPool();
		list = MusicPool.getPool();
		assertEquals(list.size(), 0);
	}
	
	
	@Test
	public void queue() {
		// Try inserting the correct musics
		try {
			
			MusicQueue.addMusic(mus1);
			Thread.sleep(10);
			MusicQueue.addMusic(mus2);
			Thread.sleep(10);
			MusicQueue.addMusic(mus3);
			Thread.sleep(10);
			MusicQueue.addMusic(mus4);
			
			assertEquals(0, mus1.getId());
			assertEquals(1, mus2.getId());
			assertEquals(2, mus3.getId());
			assertEquals(3, mus4.getId());
			
		} catch (DatabaseException e) {
			e.printStackTrace();
			fail("Error during the music insertions");
		} catch (InterruptedException e) {
			fail("Do not interrupt the tests");
		}
		
		// Try reinserting a music
		try {
			
			MusicQueue.addMusic(mus1);
			fail("The already inserted music should not be reinserted");
			
		} catch (DatabaseException e) {
			// Do nothing
		}
		
		// Test getting all the queue and verify its order
		List<Music> list = MusicQueue.getQueue();
		assertEquals(4, list.size());
		assertEquals(0, list.get(0).getId());
		assertEquals(1, list.get(1).getId());
		assertEquals(2, list.get(2).getId());
		assertEquals(3, list.get(3).getId());
		
		// Test getting the music queue head
		Music tmp = MusicQueue.pop();
		assertEquals(0, tmp.getId());
		assertEquals("Music1", tmp.getSongName());
		assertEquals("Artist1", tmp.getArtist());
		assertEquals("vid1", tmp.getVideoId());
		assertEquals(10, tmp.getDuration());
		assertEquals(mus1.getInsertDate(), tmp.getInsertDate());
		
		// Test getting a music by the video ID
		tmp = MusicQueue.getMusicByVideoId("vid4");
		assertEquals("Music4", tmp.getSongName());
		assertEquals("Artist4", tmp.getArtist());
		
		// Get the queue another time
		list = MusicQueue.getQueue();
		assertEquals(3, list.size());
		assertEquals(1, list.get(0).getId());
		assertEquals(2, list.get(1).getId());
		assertEquals(3, list.get(2).getId());
		
		// Test the deletion
		assertTrue(MusicQueue.deleteMusic(mus2));
		
		// Get the head to verify
		tmp = MusicQueue.pop();
		assertEquals(2, tmp.getId());
		assertEquals("Music3", tmp.getSongName());
	}
	
}
