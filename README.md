# Radio Disc(ord) : Une radio Discord pour les MELOMANES

Auteurs : Erik Vienne 28705180 | Emilie Siau 3700323 | Hugo Guerrier 3970839
Projet d'UE PC3R 2021.

## Comment lancer le site web

* Installer MongoDB sur sa machine
* Ouvrir sur Eclipse le projet
* Utiliser un serveur Tomcat 9.0 configuré dans Eclipse pour déployer
* Aller sur son navigateur et renter l'url "localhost:8080/radio-disc/"

## Description du projet

C'est un projet de site web communautaire permettant de piloter de manière démocratique une radio Discord. Cette radio diffuse alors sur tous les serveurs discord sur laquelle est invité la musique actuelle.
Le serveur sera divisé en trois grandes parties :
* Une partie permettant de télécharger et de convertir des musiques depuis Youtube sur demande des utilisateurs
* Une partie permettant de jouer de la musique sur le bot discord
* Une partie servant d'API RESTful pour la partie client qui sera une application monopage

Le client sera donc, comme vous avez pû le lire précédemment, un application monopage utilisant le framework Angular car c'est quand même cool.
Le client permettra à n'importe qui de proposer une musique et de voter pour les musique proposées. Toutes les x temps, la musique ayant reçu le plus de votes sera ajoutée à la queue de lecture et les votes seront réinitialisés.
Il est aussi possible pour les utilisateur de passer la musique actuelle.
Le client permettra aussi aux administrateurx de voir la queue de lecture et de la modifier, éteindre le bot ou gérer d'autres choses sensibles.

**Attention**, les vidéos seront limitées à une certaine catégorie / durée.

## Composants du serveur

* Téléchargement d'une vidéo Youtube à partir de son ID
* Conversion d'une vidéo mp4 en audio mp3
* Envoi d'un flux audio mp3 à Discord
* Gestion de la queue d'écoute avec un système de cache
* Réception des requêtes d'ajout de musique
* Réception et comptabilisation des votes
* Choix de la musique et remise à zéro des votes
* Loguer les administrateurs
* Gérer la liste de lecture
* Gérer le bot Discord
* Gérer les administrateurs

## Cas d'utilisations

Utilisateur anonyme :

* Un utilisateur anonyme se connecte au site web et vote pour une musique dans la liste des propositions
* Un utilisateur anonyme se connecte au site web et propose une musique que sera ajoutée à la liste de propositions
* Un utilisateur anonyme se connecte au site web et consulte la liste des musiques à venir

Administrateur :

* Un administrateur se connecte et se logue sur le site web et gère la liste des propositions
* Un administrateur se connecte et se logue sur le site web et gère la file des musiques en attente
* Un administrateur se connecte et se logue sur le site web et gère l'état du bot discord
* Un administrateur se connecte et se logue sur le site web et se déconnecte
* Un administrateur se connecte et se logue sur le site web et ajoute un administrateur
* Un administrateur se connecte et se logue sur le site web et met à jour son email et/ou son mot de passe
* Le super administrateur (possesseur du serveur: login="admin", password="admin") se connecte et se logue sur le site et peut ajouter des administrateurs

## Rapport sur le développement

Retrouvez le schéma du projet dans `./schema_structure.jpg`

Le développement de cette application web a été compliqué mais néanmoins un succès. Nous avons réussi à implémenter quasiment toutes les fonctionnalités que nous avions prévues à la base :

* La recherche et le téléchargement de musiques sur Youtube
* L’ajout de propositions de musiques sur le serveur
* La possibilité pour un utilisateur de voter pour une proposition
* La gestion d’administrateurs pour le serveur et le robot
* La possibilité pour un administrateur de supprimer une proposition 

Le serveur web a été implémenté à l’aide de Tomcat et de MongoDB pour tout ce qui est de la gestion de la base de données. Les dépendances du projet sont gérées à l’aide de Maven, ce qui permet de construire l’archive web facilement et d’importer le projet sur n’importe quel IDE.
Pour ce qui est de la recherche et du téléchargement de vidéos Youtube, nous utilisons l’API officielle Youtube avec des requêtes HTTP générées dans Java.
Pour la partie Discord du projet, la librairie JDA est utilisée, elle permet de se connecter à tous les serveurs sur lesquels le robot est présent et d’y jouer de la musique.

Pour la partie client, nous sommes partis à la base sur une application Angular.js, mais après avoir essayé d’apprendre sans succès cette technologie, nous nous sommes rabattus sur une application en JS pur. Ainsi, l’affichage et la gestion des éléments sur le serveur s'effectuent via des requêtes HTTP dans le code JS (utilisation de `fetch`). Cependant, nous avons rencontré un problème quant à la récupération de la file d’attente des musique (MusicQueue) depuis mongoDB. Le style du site a été travaillé un minimum pour le plaisir des yeux et éviter l'affichage HTML de base. Bien entendu, il aurait pu être fignolé mais ce n'est pas l'intérêt pincipal du projet et nous n'avions pas le temps de faire plus.

Il manque néanmoins une partie importante du projet : le robot discord permettant de jouer de la musique sur les serveurs ainsi qu’un mécanisme permettant de choisir la musique avec le plus de votes périodiquement. Etant donnée la complexité de cette tâche, il a été décidé de la mettre de côté le temps de finir les fonctionnalités “plus intéressantes” du projet du point de vue de l’UE et de ses objectifs. Nous aurions aimé implémenter cette partie pour son côté ludique, pour avoir un projet complet à présenter et pour notre satisfaction personnelle, mais le manque de temps dû à la charge de travail importante nous a rattrapés.

